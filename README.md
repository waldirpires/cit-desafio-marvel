# Desafio: #

* Crie uma API utilizando Spring MVC que orquestre duas ou mais APIs da Marvel. As chamadas as APIs da Marvel devem utilizar Jersey.

* O tema pode ser da escolha do time. O importante é que uma chamada recebida no Spring MVC gere duas ou mais chamadas as APIs da Marvel.

## Controle: ##
* src/main/java/br/com/treinamento/dojo/controller/MarvelController.java

* Não consegui finalizar todos os testes que gostaria.

## Problemas: ##
* Iniciei o projeto utilizando classes POJO geradas do Jackson, mas em função de alguns campos que não estavam sendo carregados corretamente (resourceURL), optei em utilizar JsonObject puro para as operações. Optei em deixar as classes geradas no projeto para mostrar que elas foram geradas e estão sendo usadas na hora da conversão JSON - POJO e vice-versa.

* Armazene os dados já solicitados em memória. 

* Dados armazenados em um mapa de chave(string) - objetos. Criei uma nova classe de armazenamento de dados em memória (DataServiceImpl) mas não tive tempo de incorporá-la ao projeto.

* Monte uma segunda API Restful para os dados consolidados. Ela deve contemplar os verbos HTTP.

## Controle: ##
* src/main/java/br/com/treinamento/dojo/controller/MarvelHttpCrudController.java

* Não consegui finalizar todos os testes que gostaria.

* Stack: Spring MVC 4, JAX-RS com Jersey, Jackson, Gson, JUnit, Tomcat

* Setup:
** Crie uma conta e gere um Key através do endereço: https://developer.marvel.com/account
** Clone o repositório https://bitbucket.org/andremp_cit/hands-on
** Faça os devidos testes ;)

** Pedimos a gentileza que coloque o código fonte gerado em um repositório no bitbucket para que possamos analisar (bitbucket.org)

# Repositório: #
* https://bitbucket.org/waldirpires/cit-desafio-marvel