package br.com.treinamento.dojo.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import br.com.treinamento.dojo.entity.MarvelEntityList;
import br.com.treinamento.dojo.entity.MarvelSeriesWithCharactersInfo;
import br.com.treinamento.dojo.entity.characters.MarvelCharacterResponse;
import br.com.treinamento.dojo.entity.characters.ResultElement;
import br.com.treinamento.dojo.utils.JsonUtils;
import br.com.treinamento.dojo.utils.StringUtils;

/**
 * JsonObject from payload implementation 
 * @author cmbr
 *
 */

public class MarvelSeriesClientImpl2 extends AbstractMarvelClientApi{
	final static Logger LOG = Logger.getLogger(MarvelSeriesClientImpl2.class);
	private static MarvelSeriesClientImpl2 instance;
	private Map<String, Object> data;
	
	private MarvelSeriesClientImpl2(){
		data = new HashMap<>();
	}
	
	public static MarvelSeriesClientImpl2 getInstance(){
		if (instance == null){
			instance = new MarvelSeriesClientImpl2();
		}
		return instance;
	}
	
	public JsonObject getSpecificSeries(String id) {
		return getSpecific("series", id);
	}
	
	public JsonObject getSpecificCharacter(String id) {
		return getSpecific("characters", id);
	}
	
	public JsonObject getSpecific(String operation, String id) {
		if (StringUtils.isEmpty(id)){
			return null;
		}
		operation = operation.concat("/").concat(id);
		if (data.get(operation) != null){
			LOG.info("Found local object: " + operation);
			return (JsonObject) data.get(operation);
		}
		// retrieving character from marvel server
		LOG.info("Retrieving remote object from server: " + operation);
		String str = getPayloadFromResponse(this.executeGet(operation, 1, 0));
		JsonObject o = JsonUtils.getObjectFromString(str);
		if (o.get("code").getAsInt() >=400){
			LOG.error(o);
			return null;
		}
		data.put(operation, o);
		return o;
	}
	
	/* (non-Javadoc)
	 * @see br.com.treinamento.dojo.controller.MarvelClientApi#getCompleteSeriesWithCharacters(java.lang.String)
	 */
	public MarvelSeriesWithCharactersInfo getCompleteSeriesWithCharacters(String seriesId){
		if (seriesId == null){
			return null;
		}
		// check if data already exists in object data storage
		MarvelSeriesWithCharactersInfo mswci = (MarvelSeriesWithCharactersInfo) data.get("MarvelSeriesWithCharactersInfo/"+seriesId);
		if (mswci != null){
			LOG.info("Found MarvelSeriesWithCharactersInfo with ID " + seriesId);
			return mswci;
		}
		JsonObject specificSeries = getSpecificSeries(seriesId);
		if (specificSeries == null){
			return null;
		}
		JsonObject element = JsonUtils.getUniqueObject(specificSeries);    			
		mswci = new MarvelSeriesWithCharactersInfo(element);
    	JsonArray characters = element.getAsJsonObject("characters").getAsJsonArray("items");
    	for (int i = 0; i < characters.size(); i++){
    		JsonObject chr = characters.get(i).getAsJsonObject();    		
    		if (chr.get("resourceURI") != null){
    			String characterId = JsonUtils.getIdFromResourceUri(chr, "resourceURI");
    			JsonObject character = getSpecificCharacter(characterId);
    			character = JsonUtils.getUniqueObject(character);
    			mswci.addCharacter(character);
    		}
    	}
    	data.put("MarvelSeriesWithCharactersInfo/"+seriesId, mswci);
    	LOG.info("Stored new MarvelSeriesWithCharactersInfo with ID " + seriesId);
    	return mswci;
	}
	
	public void addToData(String key, Object obj){
		data.put(key, obj);
	}
	
	public Object getData(String key){
		return data.get(key);
	}
	
	public void clearData(){
		data.clear();
	}
	
	public MarvelEntityList getAllCharacters(int maxSize, int limit){
		String operation = "characters";
		return this.getAll(operation, maxSize, limit);
	}

	public MarvelEntityList getAllSeries(int maxSize, int limit){
		String operation = "series";
		return this.getAll(operation, maxSize, limit);
	}
	
	MarvelEntityList getAll(String operation, int maxSize, int limit){
		String key = "MarvelEntityList/"+operation;
		if (data.get(key)!=null)
		{
			return (MarvelEntityList) data.get(key);
		}
		MarvelEntityList list = super.getAll(operation, maxSize, limit);
		data.put(key, list);
		return list;
		
	}

	public MarvelEntityList getAllComics(int maxSize, int limit){
		String operation = "comics";
		return this.getAll(operation, maxSize, limit);
	}

	public boolean addCharacter(JsonObject ojb){
		return addSpecific("characters", ojb);
	}

	public boolean addSeries(JsonObject obj){
		return addSpecific("series", obj);
	}

	public boolean addSpecific(String operation, JsonObject obj){
		String id = obj.get("id").getAsString();
		operation = operation.concat("/").concat(id);
		if (data.get(operation) !=null){
			LOG.error("Object already exists: " + operation);
			return false;
		}
		data.put(operation, obj);
		LOG.info("Object with ID " + operation + " added successfully");
		return true;
	}
	
	public boolean updateCharacter(JsonObject obj){
		return updateSpecific("characters", obj);
	}
	
	boolean updateSpecific(String operation, JsonObject obj){
		String id = obj.get("id").getAsString();
		operation = operation.concat("/").concat(id);
		if (data.get(operation) ==null){
			LOG.error("Object does NOT exist: " + operation);
			return false;
		}
		data.put(operation, obj);
		LOG.info("Object with ID " + operation + " updated successfully");
		return true;
	}

	public boolean deleteCharacter(JsonObject obj){
		return deleteSpecific("characters", obj);
	}
	
	boolean deleteSpecific(String operation, JsonObject obj){
		String id = obj.get("id").getAsString();
		operation = operation.concat("/").concat(id);
		if (data.get(operation) ==null){
			LOG.error("Object does NOT exist: " + operation);
			return false;
		}
		data.remove(operation);
		LOG.info("Object with ID " + operation + " removed successfully");
		return true;
	}

	public MarvelEntityList listSpecific(String operation){
		MarvelEntityList list = new MarvelEntityList();
		for (String k: data.keySet())
		{
			if (k.contains(operation)){
				list.addObject(data.get(k));
			}
		}
		LOG.info("Objects found: " + list.getList().size());
		return list;
	}
}
