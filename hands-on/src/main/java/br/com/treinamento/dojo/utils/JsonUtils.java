package br.com.treinamento.dojo.utils;

import org.springframework.http.ResponseEntity;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class JsonUtils {

	public static JsonObject getObjectFromString(String str){
		JsonParser parser = new JsonParser();
		JsonObject o = parser.parse(str).getAsJsonObject();
		return o;
	}
	
	public static JsonObject getUniqueObject(JsonObject obj){
		if (obj == null){
			return null;
		}
		if (obj.getAsJsonObject("data") == null || obj.getAsJsonObject("data").getAsJsonArray("results") == null)
		{
			return null;
		}
		if (obj.getAsJsonObject("data").getAsJsonArray("results").size() != 1){
			return null;
		}		
		return obj.getAsJsonObject("data").getAsJsonArray("results").get(0).getAsJsonObject();
	}
	
	public static String getIdFromResourceUri(JsonObject obj, String property){
		if (obj == null || property == null || property.trim().isEmpty()){
			return null;
		}
		if (obj.get(property) == null){
			return null;
		}
		String value = obj.get(property).toString().replace("\"", "");
		if (!value.startsWith("http://gateway.marvel.com/v1/public/")){
			return null;
		}
		String[] split = obj.get(property).toString().replace("\"", "").split("/");
		String id = split[split.length-1];
		return id;
	}
	
	public static String convertObjToJson(Object obj){
		Gson gson = new Gson();
        return gson.toJson(obj);		
	}
}
