package br.com.treinamento.dojo.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class DataServiceImpl {

	private Map<String, Object> data;
	
	public DataServiceImpl(){
		this.data = new HashMap<>();
	}
	
	public void addToData(String key, Object obj){
		data.put(key, obj);
	}
	
	public Object getData(String key){
		return data.get(key);
	}
	
	public void clearData(){
		data.clear();
	}
	
	public Set<Object> list(String match){
		Set<Object> list = new HashSet<>();
		for (String k: data.keySet())
		{
			if (k.contains(match)){
				list.add(data.get(k));
			}
		}
		return list;
	}	
}
