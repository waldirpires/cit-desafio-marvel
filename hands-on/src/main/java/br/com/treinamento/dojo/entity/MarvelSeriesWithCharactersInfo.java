package br.com.treinamento.dojo.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.gson.JsonObject;

public class MarvelSeriesWithCharactersInfo{

	public JsonObject series;
	public List<JsonObject> characters;
	
	public MarvelSeriesWithCharactersInfo(JsonObject series){
		this.series = series;
		this.characters = new ArrayList<>();
	}
	
	public void addCharacter(JsonObject character){
		this.characters.add(character);
	}
	
	public List<JsonObject> getCharacters(){
		return Collections.unmodifiableList(characters);
	}
}
