package br.com.treinamento.dojo.controller;

import javax.ws.rs.core.MediaType;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.gson.JsonObject;

import br.com.treinamento.dojo.entity.MarvelEntityList;
import br.com.treinamento.dojo.service.MarvelSeriesClientImpl2;
import br.com.treinamento.dojo.utils.JsonUtils;

@RestController
public class MarvelHttpCrudController {

    @RequestMapping(value = "/characters/", method = RequestMethod.POST)
    public ResponseEntity<Void> createCharacter(@RequestBody String body) {
    	
    	JsonObject obj = JsonUtils.getObjectFromString(body);
    	MarvelSeriesClientImpl2 restClient = MarvelSeriesClientImpl2.getInstance();
    	boolean result = restClient.addCharacter(obj);
        return new ResponseEntity<Void>(result?HttpStatus.CREATED:HttpStatus.BAD_REQUEST);
    }    

    @RequestMapping(value = "/characters/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public ResponseEntity<String> listCharacters() {
    	
    	MarvelSeriesClientImpl2 restClient = MarvelSeriesClientImpl2.getInstance();
    	MarvelEntityList listSpecific = restClient.listSpecific("characters");
        return new ResponseEntity<String>(JsonUtils.convertObjToJson(listSpecific), HttpStatus.OK);
    }    

    @RequestMapping(value = "/clearData/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public ResponseEntity<Void> clearData() {
    	
    	MarvelSeriesClientImpl2 restClient = MarvelSeriesClientImpl2.getInstance();
    	restClient.clearData();
        return new ResponseEntity<Void>(HttpStatus.OK);
    }    

    @RequestMapping(value = "/characters/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public ResponseEntity<String> getCharacter(@PathVariable("id") long id) {
    	
    	MarvelSeriesClientImpl2 restClient = MarvelSeriesClientImpl2.getInstance();
    	JsonObject specificCharacter = restClient.getSpecificCharacter(String.valueOf(id));
    	specificCharacter = specificCharacter!=null?specificCharacter:JsonUtils.getObjectFromString("{}");
        return new ResponseEntity<String>(specificCharacter.toString(), HttpStatus.OK);
    }    
    
    @RequestMapping(value = "/characters/{id}", method = RequestMethod.POST)
    public ResponseEntity<Void> updateCharacter(@PathVariable("id") long id, @RequestBody String body) {
    	
    	MarvelSeriesClientImpl2 restClient = MarvelSeriesClientImpl2.getInstance();
    	boolean result = restClient.updateCharacter(JsonUtils.getObjectFromString(body));
    	return new ResponseEntity<Void>(result?HttpStatus.CREATED:HttpStatus.BAD_REQUEST);
    }    

    @RequestMapping(value = "/characters/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteCharacter(@PathVariable("id") long id, @RequestBody String body) {
    	
    	MarvelSeriesClientImpl2 restClient = MarvelSeriesClientImpl2.getInstance();
    	boolean result = restClient.deleteCharacter(JsonUtils.getObjectFromString(body));
    	return new ResponseEntity<Void>(result?HttpStatus.CREATED:HttpStatus.BAD_REQUEST);
    }    
}
