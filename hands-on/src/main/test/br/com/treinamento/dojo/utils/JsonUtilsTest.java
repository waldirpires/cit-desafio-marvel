package br.com.treinamento.dojo.utils;

import org.junit.Assert;
import org.junit.Test;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class JsonUtilsTest {

	@Test
	public void testgetUniqueObjectSuccess(){
		String json = "{\"data\": {\"results\": [{\"id\": 1945,\"title\": \"Avengers: The Initiative (2007 - 2010)\"}]}}";
		JsonParser parser = new JsonParser();
		JsonObject obj = parser.parse(json).getAsJsonObject();
		JsonObject uniqueObject = JsonUtils.getUniqueObject(obj);
		Assert.assertNotNull(uniqueObject);
	}
	
	@Test
	public void testgetUniqueObjectNull(){
		JsonObject uniqueObject = JsonUtils.getUniqueObject(null);
		Assert.assertNull(uniqueObject);
	}
	
	@Test
	public void testgetUniqueObjectNoData(){
		String json = "{\"data2\": {\"results\": [{\"id\": 1945,\"title\": \"Avengers: The Initiative (2007 - 2010)\"}]}}";
		JsonParser parser = new JsonParser();
		JsonObject obj = parser.parse(json).getAsJsonObject();
		JsonObject uniqueObject = JsonUtils.getUniqueObject(obj);
		Assert.assertNull(uniqueObject);
	}
	
	@Test
	public void testgetUniqueObjectNoResults(){
		String json = "{\"data\": {\"results2\": [{\"id\": 1945,\"title\": \"Avengers: The Initiative (2007 - 2010)\"}]}}";
		JsonParser parser = new JsonParser();
		JsonObject obj = parser.parse(json).getAsJsonObject();
		JsonObject uniqueObject = JsonUtils.getUniqueObject(obj);
		Assert.assertNull(uniqueObject);
	}
	
	@Test
	public void testgetUniqueObjectResultsEmpty(){
		String json = "{\"data2\": {\"results\": []}}";
		JsonParser parser = new JsonParser();
		JsonObject obj = parser.parse(json).getAsJsonObject();
		JsonObject uniqueObject = JsonUtils.getUniqueObject(obj);
		Assert.assertNull(uniqueObject);
	}
	
	@Test
	public void testgetUniqueObjectResultsMoreThanOne(){
		String json = "{\"data\": {\"results2\": [{\"id\": 1945,\"title\": \"Avengers: The Initiative (2007 - 2010)\"},{\"id\": 1946,\"title\": \"Avengers: The Initiative (2007 - 2010)\"}]}}";
		JsonParser parser = new JsonParser();
		JsonObject obj = parser.parse(json).getAsJsonObject();
		JsonObject uniqueObject = JsonUtils.getUniqueObject(obj);
		Assert.assertNull(uniqueObject);
	}
	
	@Test
	public void getIdFromResourceUriSuccess(){
		String payload = "{\"resourceURI\": \"http://gateway.marvel.com/v1/public/creators/9018\",\"name\": \"Mahmud Asrar\",\"role\": \"penciller\"}";
		String expected = "9018";
		JsonParser parser = new JsonParser();
		JsonObject obj = parser.parse(payload).getAsJsonObject();
		String value = JsonUtils.getIdFromResourceUri(obj, "resourceURI");
		Assert.assertEquals(expected, value);		
	}
	
	@Test
	public void getIdFromResourceUriNullObj(){
		JsonObject obj = null;
		String value = JsonUtils.getIdFromResourceUri(obj, "resourceURI");
		Assert.assertNull(value);
	}
	
	@Test
	public void getIdFromResourceUriNullPropertyName(){
		String payload = "{\"resourceURI\": \"http://gateway.marvel.com/v1/public/creators/9018\",\"name\": \"Mahmud Asrar\",\"role\": \"penciller\"}";
		JsonParser parser = new JsonParser();
		JsonObject obj = parser.parse(payload).getAsJsonObject();
		String value = JsonUtils.getIdFromResourceUri(obj, null);
		Assert.assertNull(value);
		
		value = JsonUtils.getIdFromResourceUri(obj, "");
		Assert.assertNull(value);
		
		value = JsonUtils.getIdFromResourceUri(obj, " ");
		Assert.assertNull(value);
	}
	
	@Test
	public void getIdFromResourceUriPropertyNotExist(){
		String payload = "{\"resourceURI\": \"http://gateway.marvel.com/v1/public/creators/9018\",\"name\": \"Mahmud Asrar\",\"role\": \"penciller\"}";
		
		JsonParser parser = new JsonParser();
		JsonObject obj = parser.parse(payload).getAsJsonObject();
		String value = JsonUtils.getIdFromResourceUri(obj, "blah");
		Assert.assertNull(value);
	}
	
	@Test
	public void getIdFromResourceUriPropertyNotUri(){
		String payload = "{\"resourceURI\": \"blah\",\"name\": \"Mahmud Asrar\",\"role\": \"penciller\"}";
		
		JsonParser parser = new JsonParser();
		JsonObject obj = parser.parse(payload).getAsJsonObject();
		String value = JsonUtils.getIdFromResourceUri(obj, "resourceURI");
		Assert.assertNull(value);
	}
}
