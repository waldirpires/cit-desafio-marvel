//**********************************************************************
// Copyright (c) 2016 Telefonaktiebolaget LM Ericsson, Sweden.
// All rights reserved.
// The Copyright to the computer program(s) herein is the property of
// Telefonaktiebolaget LM Ericsson, Sweden.
// The program(s) may be used and/or copied with the written permission
// from Telefonaktiebolaget LM Ericsson or in accordance with the terms
// and conditions stipulated in the agreement/contract under which the
// program(s) have been supplied.
// **********************************************************************
package br.com.treinamento.dojo.service;

import java.util.Set;

import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;

import br.com.treinamento.dojo.entity.characters.MarvelCharacterResponse;
import br.com.treinamento.dojo.entity.characters.ResultElement;
import br.com.treinamento.dojo.entity.series.ItemElement;
import br.com.treinamento.dojo.entity.series.MarvelSeriesResponse;
import br.com.treinamento.dojo.service.MarvelBusinessRestClient;

public class MarvelBusinessRestClientTest
{

    private MarvelBusinessRestClient client;

    @Before
    public void setup()
    {
        client = MarvelBusinessRestClient.getInstance();
    }

    @Test
    public void testGetCharacters()
    {
    	int limit = 10;
    	int offset = 0;
    	Response response = client.getCharacters(limit, offset);
    	Assert.assertEquals(200, response.getStatus());
    	String payload = client.getPayloadFromResponse(response);
    	Assert.assertNotNull(payload);
    	Gson gson = new Gson();
    	MarvelCharacterResponse mcr = gson.fromJson(payload, MarvelCharacterResponse.class);
    	Assert.assertNotNull(mcr);
    	Assert.assertEquals(limit, mcr.getData().getLimit().intValue());
    	Assert.assertTrue(mcr.getData().getResults().length == limit);
    	
    }

    @Test
    public void testGetSeries()
    {
    	int limit = 10;
    	int offset = 0;
    	Response response = client.getSeries(limit, offset);
    	Assert.assertEquals(200, response.getStatus());
    	String payload = client.getPayloadFromResponse(response);
    	Assert.assertNotNull(payload);
    	Gson gson = new Gson();
    	MarvelSeriesResponse msr = gson.fromJson(payload, MarvelSeriesResponse.class);
    	Assert.assertNotNull(msr);
    	Assert.assertEquals(limit, msr.getData().getLimit().intValue());
    	Assert.assertTrue(msr.getData().getResults().length == limit);
    	
    }
    
    @Test
    public void testgetAllCharacters(){
    	Set<ResultElement> allCharacters = client.getAllCharacters(200, 20);
    	System.out.println(allCharacters.size());
    }
    
    @Test
    public void testgetCharacter(){
    	String id = "1009165";
    	MarvelCharacterResponse character = client.getCharacter(id);
    	Assert.assertNotNull(character);
    	Assert.assertEquals(1, character.getData().getResults().length);
    	
    }
        
    @Test
    public void testResourceUriSplit(){
    	String uri = "http://gateway.marvel.com/v1/public/characters/1011334";
    	String[] split = uri.split("/");
    	System.out.println(split[split.length-1]);
    }
    
    @Test
    public void testGetSpecificSeries(){
    	String id = "1945";
    	//http://gateway.marvel.com/v1/public/series/1945?ts=1478785656836&apikey=19554a34e7f49a1956e56ce9e2cb99bc&hash=da060e1e6f6e33c4e4e63c397aa9f3fa&limit=5
    	// characters.items.resourceURI
    	
    	MarvelSeriesResponse specificSeries = client.getSpecificSeries(id);
    	Assert.assertNotNull(specificSeries);
    	Assert.assertEquals(1, specificSeries.getData().getResults().length);
    	br.com.treinamento.dojo.entity.series.ResultElement series = specificSeries.getData().getResults()[0];
    	for (int i = 0; i < series.getCharacters().getItems().length; i++){
    		ItemElement itemElement = series.getCharacters().getItems()[i];
    		if (itemElement.getResourceURI() != null){
    			String[] split = itemElement.getResourceURI().split("//");
    			String characterId = split[split.length-1];
    			MarvelCharacterResponse character = client.getCharacter(characterId);
    			System.out.println(character.getData().getResults()[0].getName());
    		}
    	}
    }
    
    
}
